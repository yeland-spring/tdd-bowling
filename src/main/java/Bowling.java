import java.util.ArrayList;
import java.util.List;

public class Bowling {

    private List<Frame> frames = new ArrayList<>();

    public void addFrames(List frames) {
        this.frames.addAll(frames);
    }

    public List<Frame> getFrames() {
        return frames;
    }

    public Integer calculateScore() {
        if (frames.size() == 0) {
            return 0;
        }
        Integer basicScore = getBasicScore();
        return basicScore + getBonus();
    }

    private Integer getBasicScore() {
        return frames.stream().mapToInt(frame -> frame.getFirstScore()+frame.getSecondScore()).limit(10).sum();
    }

    private Integer getBonus() {
        Integer bonus = 0;
        for (int i = 0; i < frames.size(); i++) {
            if (frames.get(i).getStatus().equals("spare")) {
                bonus += frames.get(i + 1).getFirstScore();
            }
            if (frames.get(i).getStatus().equals("strike") && i < 10) {
                bonus += getStrikeBonus(i);
            }
        }
        return bonus;
    }

    private Integer getStrikeBonus(int i) {
        int bonus = 0;
        if (frames.get(i + 1).getSecondScore() == 0) {
            bonus += (frames.get(i + 1).getFirstScore() + frames.get(i + 2).getFirstScore());
        } else {
            bonus += (frames.get(i + 1).getFirstScore() + frames.get(i + 1).getSecondScore());
        }
        return bonus;
    }

}
