public class Frame {
    private Integer firstScore;
    private Integer secondScore;
    private String status = "pins";

    public Frame(Integer firstScore, Integer secondScore) {
        this.firstScore = firstScore;
        this.secondScore = secondScore;
        setStatus();
    }

    public Frame(Integer firstScore) {
        this(firstScore, 0);
    }

    public Integer getFirstScore() {
        return firstScore;
    }

    public Integer getSecondScore() {
        return secondScore;
    }

    public void setStatus() {
        if (firstScore + secondScore == 10) {
            this.status = "spare";
        }
        if (firstScore == 10) {
            this.status = "strike";
        }
    }

    public String getStatus() {
        return status;
    }
}
